# Concevoir une application web dynamique avec `R` et `shinydashboard`

Supports associés à la formation nationale **Concevoir une application Web dynamique avec `R` et `shinydashboard`**.

**EN COURS DE REDACTION**  
*Ceci est une version provisoire des supports.*

*Le code source est disponible :*  
- [ici, pour le bookdown](https://gitlab.com/CTassart/formation-rshiny-bookdown).
- [ici, pour le diaporama](https://gitlab.com/CTassart/formation-rshiny)
- [Pour le personnel Insee](https://gitlab.insee.fr/t19qhl/formation-rshiny-bookdown/-/tree/master) 

*Les supports de formation :*  
- [le livret de la formation (`R bookdown`)](https://ctassart.gitlab.io/formation-rshiny-bookdown).
- [le diaporama de la formation](https://ctassart.gitlab.io/formation-rshiny/formation-shiny-bookdown#1)

Bien que largement remaniée, cette formation a été élaborée à partir de la formation dispensée par les formateurs de la Direction Régionale de l'Institut National de la Statistique et des Etudes Economiques des Hauts-de-France.

---

## **Plan de la formation**
### 1. Introduction
### 2. Présentation rapide de Shiny
### 3. Un exemple pas à pas
### 4. Interaction entre l’interface et le programme
### 5. Gestion de la mise en page d’une application
### 6. Les fonctions du server
### 7. shinyfiles et interfaces dynamiques
### 8. Bon à savoir

---

## Contributeurs

  * **Chapitre 1. :** Cédric Tassart
  * **Chapitre 2. :** ensemble du GC
  * **Chapitre 3. :** Raymond Warnod
  * **Chapitre 4. :** ensemble du GC
  * **Chapitre 5. :** Cédric Tassart
  * **Chapitre 6. :** ensemble du GC
  * **Chapitre 7. :** Raymond Warnod
  * **Chapitre 8. :** ensemble du GC
    - *Scinder le code source avec Golem :* Cédric Tassart,
    - *Créer un exécutable :* Cédric Tassart.
    - *Créer un raccourci :* Raymond Warnod,
  * **Les exerices :** Issam Belkhadi, Raymond Warnod, Cédric Tassart
  * **La relecture du document :** ensemble du GC
  * **pour l'animation du GC :** Georges Pavlov